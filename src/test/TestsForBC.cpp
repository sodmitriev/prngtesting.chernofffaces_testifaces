#include "gtest/gtest.h"
#include "../chernofffaces/BC_ControlComponent.h"
#include "../chernofffaces/BC_ValidationOfParameters.h"
#include "../chernofffaces/BC_OpeningFileAndParsing.h"
#include <iostream>

// |���� 1| ������ TestControlComponent
// ���� 1.1 ValidationOfParameters ��� ��������� ����������
TEST(TestControlComponent, ValidationOfParametersIsTrue) {
	BO_SequenceParameters SeqPar("F:\\1\\AIS\\sts-2.1.2\\sts-2.1.2\\data\\1.bin", 3, 0, 0, 2);
	BC_ControlComponent control;
	//std::cout << "control:" << control.ValidationOfParameters(&SeqPar) << std::endl;
	EXPECT_EQ(control.ValidationOfParameters(&SeqPar), 1); 
}

// ���� 1.2 ValidationOfParameters ����� ����� = 0
TEST(TestControlComponent, FileLengthIsNull) {
	BO_SequenceParameters SeqPar("", 5, 0, 0, 2);
	BC_ControlComponent control;
	EXPECT_EQ(control.ValidationOfParameters(&SeqPar), 2); 
}

// ���� 1.3 ValidationOfParameters ���� �� ����������
TEST(TestControlComponent, FileIsNotExists) {
	BO_SequenceParameters SeqPar("F:\\1\\AIS\\sts-2.1.2\\sts-2.1.2\\data\\15.bin", 5, 0, 0, 2);
	BC_ControlComponent control;
	EXPECT_EQ(control.ValidationOfParameters(&SeqPar), 3);
}

// ���� 1.4 ValidationOfParameters ��� ������ 0
TEST(TestControlComponent, StepIsNegative) {
	BO_SequenceParameters SeqPar("F:\\1\\AIS\\sts-2.1.2\\sts-2.1.2\\data\\1.bin", 5, 0, 0, -2);
	BC_ControlComponent control;
	EXPECT_EQ(control.ValidationOfParameters(&SeqPar), 4);
}

// ���� 1.5 ValidationOfParameters ����� ������������������ ������ 0
TEST(TestControlComponent, LengthSequenceIsNegative) {
	BO_SequenceParameters SeqPar("F:\\1\\AIS\\sts-2.1.2\\sts-2.1.2\\data\\1.bin", -100, 0, 0, 2);
	BC_ControlComponent control;
	EXPECT_EQ(control.ValidationOfParameters(&SeqPar), 5); 
}

// ���� 1.6 ValidationOfParameters  ����� ������������������ ��������� ����� ����� - �� ����� ���������
TEST(TestControlComponent, LengthSequenceMoreThanSizeOfFile) {
	BO_SequenceParameters SeqPar("F:\\1\\AIS\\sts-2.1.2\\sts-2.1.2\\data\\1.bin", 100, 0, 0, 2);
	BC_ControlComponent control;
	EXPECT_EQ(control.ValidationOfParameters(&SeqPar), 6); 
}

// ���� 1.7 ValidationOfParameters ��� ������ ����� ������������������ - �� ����� ���������
TEST(TestControlComponent, StepMoreThanLengthSequence) {
	BO_SequenceParameters SeqPar("F:\\1\\AIS\\sts-2.1.2\\sts-2.1.2\\data\\1.bin", 5, 0, 0, 6);
	BC_ControlComponent control;
	EXPECT_EQ(control.ValidationOfParameters(&SeqPar), 7); 
}

// ���� 1.8 �������� �� ������ ����
TEST(TestControlComponent, FileIsEmpty) {
	BO_SequenceParameters SeqPar("F:\\1\\AIS\\sts-2.1.2\\sts-2.1.2\\data\\2.bin", 4, 0, 0, 2);
	BC_ControlComponent control;
	EXPECT_EQ(control.ValidationOfParameters(&SeqPar), 8);
}

// ���� 1.9 �������� �� �������� � �������
TEST(TestControlComponent, OpenAndParsing) {
	BO_SequenceParameters SeqPar("F:\\1\\AIS\\sts-2.1.2\\sts-2.1.2\\data\\2.bin", 4, 0, 0, 2);
	BC_ControlComponent control;
	EXPECT_EQ(control.OpeningFileAndParsing(&SeqPar), 1);
}

// ���� 1.10 �������� ���������� �������������� ������ 
TEST(TestControlComponent, ValuesForStatisticalTests) {
	BO_SequenceParameters SeqPar("F:\\1\\AIS\\sts-2.1.2\\sts-2.1.2\\data\\tmp2", 4, 0, 0, 3);
	BC_ControlComponent control;
	control.OpeningFileAndParsing(&SeqPar);
	//std::cout << "GetNumberOfIterationsVer1:" << SeqPar.GetNumberOfIterationsVer1() << std::endl;
	//std::cout << "GetNumberOfIterationsVer2:" << SeqPar.GetNumberOfIterationsVer2() << std::endl;

	// ������������� ��� ����������� �������������� ������
	//================================================
	//BO_ResultsTest RTforRI[15];

	BO_ResultsIteration *resultsiterationVer1 = new BO_ResultsIteration[SeqPar.GetNumberOfIterationsVer1()];
	BO_ResultsIteration *resultsiterationVer2 = new BO_ResultsIteration[SeqPar.GetNumberOfIterationsVer2()];
	/*
	//��������� ������ ���� ResultsTest
	for (int i = 0; i < 15; i++)
	{
		RTforRI[i].SetResultTest(0.00);
		RTforRI[i].SetTestNumber(i);
	}
	
	//��������� ������ ���� ResultsIteration1
	for (int i = 0; i < SeqPar.GetNumberOfIterationsVer1(); i++)
	{
		resultsiterationVer1[i].SetResultIteration(RTforRI);
	}
	
	//��������� ������ ���� ResultsIteration2
	for (int i = 0; i < SeqPar.GetNumberOfIterationsVer2(); i++)
	{
		resultsiterationVer2[i].SetResultIteration(RTforRI);
	}*/

	BO_ResultsExperiment experiment1(resultsiterationVer1, SeqPar.GetNumberOfIterationsVer1());
	BO_ResultsExperiment experiment2(resultsiterationVer2, SeqPar.GetNumberOfIterationsVer2());
	//================================================

	EXPECT_EQ(control.ExecutionStatisticalTests(&SeqPar, &experiment1, &experiment2), 0);

	// ���������� ������������
	BO_ResultsExperiment *GetRE1, *GetRE2;
	BO_ResultsIteration *GetRI;
	BO_ResultsTest *GetRT;
	// Experiment1
	GetRI = experiment1.GetResultsExperiment();
	for (int i = 0; i < SeqPar.GetNumberOfIterationsVer1(); i++)
	{
		GetRT= GetRI[i].GetResultIteration();
		for (int j = 0; j < 15; j++) //����� ������� �����������
		{
			std::cout << "TestNumber[" << j << "]:" << GetRT[j].GetTestNumber() << "\n";
			std::cout << "ResultVer1[" << j << "]:" << GetRT[j].GetResultTest() << "\n";
		}
	}

	std::cout << "===============================================" << std::endl;
	// Experiment2
	GetRI = experiment2.GetResultsExperiment();
	for (int i = 0; i < SeqPar.GetNumberOfIterationsVer2(); i++)
	{
		GetRT = GetRI[i].GetResultIteration();
		for (int j = 0; j < 15; j++) //����� ������� �����������
		{
			std::cout << "TestNumber[" << j << "]:" << GetRT[j].GetTestNumber() << "\n";
			std::cout << "ResultVer2[" << j << "]:" << GetRT[j].GetResultTest() << "\n";
		}
	}
}

// ���� 1.11 �������� �� ���������� ����������� ������������� ������ � ��
TEST(TestControlComponent, SaveToDB) {
	BO_SequenceParameters SeqPar("F:\\1\\AIS\\sts-2.1.2\\sts-2.1.2\\data\\tmp2", 4, 0, 0, 3);
	BC_ControlComponent control;
	control.OpeningFileAndParsing(&SeqPar);
	std::cout << "GetNumberOfIterationsVer1:" << SeqPar.GetNumberOfIterationsVer1() << std::endl;
	std::cout << "GetNumberOfIterationsVer2:" << SeqPar.GetNumberOfIterationsVer2() << std::endl;

	// ������������� ��� ����������� �������������� ������
	//================================================

	BO_ResultsIteration *resultsiterationVer1 = new BO_ResultsIteration[SeqPar.GetNumberOfIterationsVer1()];
	BO_ResultsIteration *resultsiterationVer2 = new BO_ResultsIteration[SeqPar.GetNumberOfIterationsVer2()];

	BO_ResultsExperiment experiment1(resultsiterationVer1, SeqPar.GetNumberOfIterationsVer1());
	BO_ResultsExperiment experiment2(resultsiterationVer2, SeqPar.GetNumberOfIterationsVer2());
	//================================================

	control.ExecutionStatisticalTests(&SeqPar, &experiment1, &experiment2);
	EXPECT_EQ(control.SaveToDB(&experiment1, &experiment2),0);
}

// ���� 1.12 �������� �������� �������� ��� ��������� ���
TEST(TestControlComponent, GenerationFace) {
	BO_SequenceParameters SeqPar("F:\\1\\AIS\\sts-2.1.2\\sts-2.1.2\\data\\tmp2", 4, 0, 0, 3);
	BC_ControlComponent control;
	control.OpeningFileAndParsing(&SeqPar);
	std::cout << "GetNumberOfIterationsVer1:" << SeqPar.GetNumberOfIterationsVer1() << std::endl;
	std::cout << "GetNumberOfIterationsVer2:" << SeqPar.GetNumberOfIterationsVer2() << std::endl;

	// ������������� ��� ����������� �������������� ������
	//================================================

	BO_ResultsIteration *resultsiterationVer1 = new BO_ResultsIteration[SeqPar.GetNumberOfIterationsVer1()];
	BO_ResultsIteration *resultsiterationVer2 = new BO_ResultsIteration[SeqPar.GetNumberOfIterationsVer2()];

	BO_ResultsExperiment experiment1(resultsiterationVer1, SeqPar.GetNumberOfIterationsVer1());
	BO_ResultsExperiment experiment2(resultsiterationVer2, SeqPar.GetNumberOfIterationsVer2());
	//================================================
	control.ExecutionStatisticalTests(&SeqPar, &experiment1, &experiment2); // ���������� ������

	BO_FaceParametr * Faceparametr = new BO_FaceParametr[15];// ������ FaceParametr for Face ������ 15
	
	//������� ������� ��� ��� ������� ��������
	BO_Face * faceVer1 = new BO_Face[SeqPar.GetNumberOfIterationsVer1()];
	BO_Face * faceVer2 = new BO_Face[SeqPar.GetNumberOfIterationsVer2()];

	// ���������� ������������
	BO_ResultsExperiment *GetRE1, *GetRE2;
	BO_ResultsIteration *GetRI;
	BO_ResultsTest *GetRT;
	// Experiment1
	GetRI = experiment1.GetResultsExperiment();
	for (int i = 0; i < SeqPar.GetNumberOfIterationsVer1(); i++)
	{
		GetRT = GetRI[i].GetResultIteration();
		for (int j = 0; j < 15; j++) //����� ������� �����������
		{
			
			Faceparametr[i].SetParametr(GetRT[j]);
			Faceparametr[i].SetNameParametr("Parametr1");
			//std::cout << "TestNumber[" << j << "]:" << GetRT[j].GetTestNumber() << "\n";
			//std::cout << "ResultVer1[" << j << "]:" << GetRT[j].GetResultTest() << "\n";
		}
		faceVer1[i].SetFaceParameters(Faceparametr);
		faceVer1[i].SetFaceName("Face1");
	}

	std::cout << "===============================================" << std::endl;
	// Experiment2
	GetRI = experiment2.GetResultsExperiment();
	for (int i = 0; i < SeqPar.GetNumberOfIterationsVer2(); i++)
	{
		GetRT = GetRI[i].GetResultIteration();
		for (int j = 0; j < 15; j++) //����� ������� �����������
		{
			Faceparametr[i].SetParametr(GetRT[j]);
			Faceparametr[i].SetNameParametr("Parametr2");
			std::cout << "TestNumber[" << j << "]:" << GetRT[j].GetTestNumber() << "\n";
			std::cout << "ResultVer2[" << j << "]:" << GetRT[j].GetResultTest() << "\n";
		}
		faceVer2[i].SetFaceParameters(Faceparametr);
		faceVer2[i].SetFaceName("Face2");
	}

	EXPECT_EQ(control.GenerationFace(faceVer1, faceVer2), 0);
	delete[] Faceparametr;
}

// ���� 1.13 �������� ���������� � �� ���
TEST(TestControlComponent, SaveToDBFaces) {
	BO_SequenceParameters SeqPar("F:\\1\\AIS\\sts-2.1.2\\sts-2.1.2\\data\\tmp2", 4, 0, 0, 3);
	BC_ControlComponent control;
	control.OpeningFileAndParsing(&SeqPar);
	std::cout << "GetNumberOfIterationsVer1:" << SeqPar.GetNumberOfIterationsVer1() << std::endl;
	std::cout << "GetNumberOfIterationsVer2:" << SeqPar.GetNumberOfIterationsVer2() << std::endl;

	// ������������� ��� ����������� �������������� ������
	//================================================

	BO_ResultsIteration *resultsiterationVer1 = new BO_ResultsIteration[SeqPar.GetNumberOfIterationsVer1()];
	BO_ResultsIteration *resultsiterationVer2 = new BO_ResultsIteration[SeqPar.GetNumberOfIterationsVer2()];

	BO_ResultsExperiment experiment1(resultsiterationVer1, SeqPar.GetNumberOfIterationsVer1());
	BO_ResultsExperiment experiment2(resultsiterationVer2, SeqPar.GetNumberOfIterationsVer2());
	//================================================
	control.ExecutionStatisticalTests(&SeqPar, &experiment1, &experiment2); // ���������� ������

	BO_FaceParametr * Faceparametr = new BO_FaceParametr[15];// ������ FaceParametr for Face ������ 15

	//������� ������� ��� ��� ������� ��������
	BO_Face * faceVer1 = new BO_Face[SeqPar.GetNumberOfIterationsVer1()];
	BO_Face * faceVer2 = new BO_Face[SeqPar.GetNumberOfIterationsVer2()];

	// ���������� ������������
	BO_ResultsExperiment *GetRE1, *GetRE2;
	BO_ResultsIteration *GetRI;
	BO_ResultsTest *GetRT;
	// Experiment1
	GetRI = experiment1.GetResultsExperiment();
	for (int i = 0; i < SeqPar.GetNumberOfIterationsVer1(); i++)
	{
		GetRT = GetRI[i].GetResultIteration();
		for (int j = 0; j < 15; j++) //����� ������� �����������
		{

			Faceparametr[i].SetParametr(GetRT[j]);
			Faceparametr[i].SetNameParametr("Parametr1");
			//std::cout << "TestNumber[" << j << "]:" << GetRT[j].GetTestNumber() << "\n";
			//std::cout << "ResultVer1[" << j << "]:" << GetRT[j].GetResultTest() << "\n";
		}
		faceVer1[i].SetFaceParameters(Faceparametr);
		faceVer1[i].SetFaceName("Face1");
	}

	std::cout << "===============================================" << std::endl;
	// Experiment2
	GetRI = experiment2.GetResultsExperiment();
	for (int i = 0; i < SeqPar.GetNumberOfIterationsVer2(); i++)
	{
		GetRT = GetRI[i].GetResultIteration();
		for (int j = 0; j < 15; j++) //����� ������� �����������
		{
			Faceparametr[i].SetParametr(GetRT[j]);
			Faceparametr[i].SetNameParametr("Parametr2");
			std::cout << "TestNumber[" << j << "]:" << GetRT[j].GetTestNumber() << "\n";
			std::cout << "ResultVer2[" << j << "]:" << GetRT[j].GetResultTest() << "\n";
		}
		faceVer2[i].SetFaceParameters(Faceparametr);
		faceVer2[i].SetFaceName("Face2");
	}

	EXPECT_EQ(control.SaveToDB(faceVer1, faceVer2), 0);
	delete[] Faceparametr;
}

// ���� 1.13 �������� ��������� ������
TEST(TestControlComponent, GenerationReport) {
	BC_ControlComponent control;
	control.
}
