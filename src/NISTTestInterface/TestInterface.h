//
// Created by svuatoslav on 12/15/18.
//

#ifndef NISTSTIFACE_TESTINTERFACE_H
#define NISTSTIFACE_TESTINTERFACE_H

#include <vector>
#include <string>

class TestInterface{
    mutable std::vector<double> _lastResult;
protected:
    virtual std::vector<double> runTest(const std::vector<bool> & sequence) const = 0;
public:
    enum modes : int
    {
        min = -1,
        max = -2,
        avg = -3,
        mean = -4,
        random = -5,

        val0 = 0,
        val1 = 1,
        val2 = 2,
        val3 = 3,
        val4 = 4,
        val5 = 5,
        val6 = 6,
        val7 = 7,
        val8 = 8,
        val9 = 9,
        val10 = 10,
        val11 = 11,
        val12 = 12,
        val13 = 13,
        val14 = 14,
        val15 = 15,
        val16 = 16,
        val17 = 17,
    };
    static std::vector<bool> readSequence(const std::string & sequenceFile);
    TestInterface() = default;
    virtual ~TestInterface() = default;
    void test(const std::vector<bool> & sequence) const;
    inline void test(const std::string & sequenceFile) const
    {
        test(readSequence(sequenceFile));
    }
    double getResult(modes mode) const;
    void dropResults();
};


#endif //NISTSTIFACE_TESTINTERFACE_H
