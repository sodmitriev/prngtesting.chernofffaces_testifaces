//
// Created by svuatoslav on 12/15/18.
//

#include "XMLParse.h"

#include <tinyxml.h>

void XMLParse::load(const std::string &configFile, const std::string &elementName)
{
    TiXmlDocument doc;
    if(!doc.LoadFile(configFile))
    {
        throw std::runtime_error("Failed to open config file \""+ configFile +"\" : " + doc.ErrorDesc());
    }
    TiXmlElement *el = doc.RootElement();
    if(el == nullptr)
    {
        throw std::runtime_error("No root element in config file");
    }
    el = el->FirstChildElement(elementName);
    if(el == nullptr)
    {
        throw std::runtime_error("No \"" + elementName + "\" element in config file");
    }
    for (auto attr = el->FirstAttribute(); attr != nullptr; attr = attr->Next())
    {
        const char * name = attr->Name();
        const char * val = attr->Value();
        if(name != nullptr && val != nullptr)
        {
            _values.emplace(name, val);
        }
    }
}

std::string XMLParse::attribute(const std::string &name) const
{
    if(_values.find(name) == _values.cend())
    {
        throw std::runtime_error("\"" + name + "\" was not found in the configurations");
    }
    return _values.at(name);
}
