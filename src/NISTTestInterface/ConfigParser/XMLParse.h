//
// Created by svuatoslav on 12/15/18.
//

#ifndef NISTSTIFACE_XMLPARSE_H
#define NISTSTIFACE_XMLPARSE_H


#include <string>
#include <unordered_map>

class XMLParse{
    std::unordered_map<std::string, std::string> _values;
public:
    XMLParse() = default;
    void load(const std::string & configFile, const std::string & elementName);
    std::string attribute(const std::string & name) const;
};


#endif //NISTSTIFACE_XMLPARSE_H
