cmake_minimum_required(VERSION 3.7)
project(niststpp)

set(CMAKE_CXX_STANDARD 14)

include_directories(dfft/include)
add_subdirectory(dfft)

include_directories(stat_funcs/include)
add_subdirectory(stat_funcs)