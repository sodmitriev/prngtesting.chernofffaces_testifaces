#include <cmath>

#include "cephes.h"
#include "stat_fncs.h"

extern "C"
{
#include "dfft.h"
}

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
         D I S C R E T E  F O U R I E R  T R A N S F O R M  T E S T 
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
 
double
DiscreteFourierTransform(const std::vector<bool>& seq)
{
	int		count, ifac[15];
    std::vector<double> X(seq.size(), 0);
    std::vector<double> wsave(seq.size() * 2, 0);
    std::vector<double> m(seq.size()/2 + 1);
	for (size_t i = 0; i < seq.size(); ++i)
		X[i] = seq[i] ? 1 : -1;

	__ogg_fdrffti(static_cast<int>(seq.size()), wsave.data(), ifac);		/* INITIALIZE WORK ARRAYS */
	__ogg_fdrfftf(static_cast<int>(seq.size()), X.data(), wsave.data(), ifac);	/* APPLY FORWARD FFT */

	m[0] = fabs(X[0]);
	for (int i=0; i<m.size() - 1; i++ )
		m[i + 1] = sqrt(pow(X[2*i+1],2)+pow(X[2*i+2],2));
	count = 0;				       /* CONFIDENCE INTERVAL */
	double upperBound = sqrt(2.995732274*seq.size());
	for (int i=0; i<seq.size()/2; i++ )
		if ( m[i] < upperBound )
			count++;
	return erfc(fabs(count - (0.95*seq.size()/2.0))/sqrt(seq.size()/4.0*0.95*0.05)/sqrt(2.0));
}
