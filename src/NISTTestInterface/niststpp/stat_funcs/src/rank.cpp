#include <cmath>

#include "cephes.h"
#include "stat_fncs.h"
#include "matrix.h"

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
                              R A N K  T E S T
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

double
Rank(const std::vector<bool>& seq)
{
	double		product, p_32, p_31, p_30, F_32, F_31, F_30;
	matrix	    matr(32, 32);
	size_t N = seq.size() / (32 * 32);
	if ( N == 0 ) {
	    throw std::logic_error("Insufficient # Of Bits To Define An 32x32 Matrix");
	}
    int r = 32;					/* COMPUTE PROBABILITIES */
    product = 1;
    for (int i=0; i<=r-1; i++ )
        product *= ((1.e0-pow(2, i-32))*(1.e0-pow(2, i-32)))/(1.e0-pow(2, i-r));
    p_32 = pow(2, r*(32+32-r)-32*32) * product;

    r = 31;
    product = 1;
    for (int i=0; i<=r-1; i++ )
        product *= ((1.e0-pow(2, i-32))*(1.e0-pow(2, i-32)))/(1.e0-pow(2, i-r));
    p_31 = pow(2, r*(32+32-r)-32*32) * product;

    p_30 = 1 - (p_32+p_31);

    F_32 = 0;
    F_31 = 0;
    for (int k=0; k<N; k++ ) {			/* FOR EACH 32x32 MATRIX   */
        std::vector<bool> subvec(seq.cbegin(), seq.cend());
        matr.def_matrix(std::vector<bool>(seq.cbegin() + k * 32 * 32, seq.cbegin() + (k + 1) * 32 * 32));
        auto R = matr.computeRank();
        if ( R == 32 )
            F_32++;			/* DETERMINE FREQUENCIES */
        if ( R == 31 )
            F_31++;
    }
    F_30 = (double)N - (F_32+F_31);
    return exp(-((pow(F_32 - N*p_32, 2)/(N*p_32) +
                  pow(F_31 - N*p_31, 2)/(N*p_31) +
                  pow(F_30 - N*p_30, 2)/(N*p_30)))/2.e0);
}
