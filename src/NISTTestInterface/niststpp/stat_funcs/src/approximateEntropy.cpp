#include <cmath>

#include "cephes.h"
#include "stat_fncs.h"

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
                A P P R O X I M A T E  E N T R O P Y   T E S T
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

double
ApproximateEntropy(const std::vector<bool>& sequence, size_t blockLength)
{
	int				index;
	double			ApEn[2], apen, chi_squared;

	int r = 0;
	for (size_t blockSize=blockLength; blockSize<=blockLength+1; blockSize++ ) {
		if ( blockSize == 0 ) {
			ApEn[0] = 0.00;
			r++;
		}
		else {
			std::vector<unsigned int> P(static_cast<unsigned long>(pow(2, blockSize + 1) - 1), 0);

			for (size_t i=0; i < sequence.size(); i++ ) { /* COMPUTE FREQUENCY */
				size_t k = 1;
				for (size_t j=0; j<blockSize; j++ ) {
					k <<= 1;
					if ( (int)sequence[(i+j) % sequence.size()] == 1 )
						k++;
				}
				P[k-1]++;
			}
			/* DISPLAY FREQUENCY */
			double sum = 0.0;
			index = (int)pow(2, blockSize)-1;
			for (size_t i=0; i<(size_t)pow(2, blockSize); i++ ) {
				if ( P[index] > 0 )
					sum += P[index]*log(((double)P[index])/sequence.size());
				index++;
			}
			sum /= sequence.size();
			ApEn[r] = sum;
			r++;
		}
	}
	apen = ApEn[0] - ApEn[1];
	
	chi_squared = 2.0*sequence.size()*(log(2) - apen);
	return cephes_igamc(pow(2, blockLength-1), chi_squared/2.0);
}