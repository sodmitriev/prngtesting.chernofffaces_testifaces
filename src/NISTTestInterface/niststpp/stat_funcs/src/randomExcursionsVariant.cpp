#include <cmath>

#include "cephes.h"
#include "stat_fncs.h"

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
            R A N D O M  E X C U R S I O N S  V A R I A N T  T E S T
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

std::array<double, 18>
RandomExcursionsVariant(const std::vector<bool>& seq)
{
	int		J, x, count;
	int		stateX[18] = { -9, -8, -7, -6, -5, -4, -3, -2, -1, 1, 2, 3, 4, 5, 6, 7, 8, 9 };

	std::vector<int> S_k(seq.size(), 0);

	J = 0;
	S_k[0] = 2*(int)seq[0] - 1;
	for (size_t i=1; i<seq.size(); i++ ) {
		S_k[i] = S_k[i-1] + 2*seq[i] - 1;
		if ( S_k[i] == 0 )
			J++;
	}
	if ( S_k[seq.size()-1] != 0 )
		J++;
	std::array<double, 18> ret {};
    for (size_t p=0; p<=17; p++ ) {
        x = stateX[p];
        count = 0;
        for (size_t i=0; i<seq.size(); i++ )
            if ( S_k[i] == x )
                count++;
        ret[p] = erfc(fabs(count-J)/(sqrt(2.0*J*(4.0*fabs(x)-2))));
    }
    return ret;
}
