#include <cmath>

#include "cephes.h"
#include "stat_fncs.h"

double psi2(const std::vector<bool>& seq, size_t blockLength);

std::pair<double, double>
Serial(const std::vector<bool>& seqence, size_t blockLength)
{
	double	psim0, psim1, psim2, del1, del2;
	
	psim0 = psi2(seqence, blockLength);
	psim1 = psi2(seqence, blockLength-1);
	psim2 = psi2(seqence, blockLength-2);
	del1 = psim0 - psim1;
	del2 = psim0 - 2.0*psim1 + psim2;
    return std::make_pair(cephes_igamc(pow(2, blockLength-1)/2, del1/2.0), cephes_igamc(pow(2, blockLength-2)/2, del2/2.0));
}

double
psi2(const std::vector<bool>& seq, size_t blockLength)
{
	if (blockLength == 0)
		return 0.0;

    std::vector<unsigned int> P(static_cast<unsigned long>(pow(2, blockLength + 1) - 1), 0);

	for (size_t i=0; i<seq.size(); i++ ) {		 /* COMPUTE FREQUENCY */
		size_t k = 1;
		for (size_t j=0; j<blockLength; j++ ) {
			if ( !seq[(i+j)%seq.size()] )
				k *= 2;
			else
				k = 2*k+1;
		}
		P[k-1]++;
	}
	double sum = 0.0;
	for (int i=(int)pow(2, blockLength)-1; i<(int)pow(2, blockLength+1)-1; i++ )
		sum += pow(P[i], 2);
	sum = (sum * pow(2, blockLength)/(double)seq.size()) - (double)seq.size();
	
	return sum;
}
