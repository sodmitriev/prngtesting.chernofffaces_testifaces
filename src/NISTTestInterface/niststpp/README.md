NIST Statistical Tests for C++
=====

CPP-style implementation of NIST staticstical tests.
This project is based on the official [NIST Statistical Test Suit](https://csrc.nist.gov/projects/random-bit-generation/documentation-and-software).
Unlike the original project, this lightweight implementation provides only a set of functions to test bit sequences.

Please note the original software disclaimer:

Software disclaimer: "This software was developed at the National Institute of Standards and Technology by employees of the Federal Government in the course of their official duties. Pursuant to title 17 Section 105 of the United States Code this software is not subject to copyright protection and is in the public domain. The NIST Statistical Test Suite is an experimental system. NIST assumes no responsibility whatsoever for its use by other parties, and makes no guarantees, expressed or implied, about its quality, reliability, or any other characteristic. We would appreciate acknowledgment if the software is used."