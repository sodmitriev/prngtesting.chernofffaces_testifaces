#ifndef STSPP_DFFT_H
#define STSPP_DFFT_H

void __ogg_fdrffti(int n, double *wsave, int *ifac);
void __ogg_fdrfftf(int n, double *r, double *wsave, const int *ifac);

#ifdef LBBBBBBBBBBBBBBBB
void __ogg_fdcosqf(int n, double *x, double *wsave, int *ifac);
void __ogg_fdcosqb(int n, double *x, double *wsave, int *ifac);
#endif

#endif //STSPP_DFFT_H
