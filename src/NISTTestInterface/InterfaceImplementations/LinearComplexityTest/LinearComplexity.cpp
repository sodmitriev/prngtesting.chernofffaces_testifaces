//
// Created by svuatoslav on 12/15/18.
//

#include <InterfaceImplementations/include/LinearComplexity.h>

#include "LinearComplexity.h"
#include "stat_fncs.h"
#include "XMLParse.h"

const char* LinearComplexityTest::TestName = "LinearComplexity";

std::vector<double> LinearComplexityTest::runTest(const std::vector<bool> &sequence) const
{
    if(_blockSize == 0)
    {
        throw std::logic_error( std::string(TestName) + " test is not loaded");
    }
    return {LinearComplexity(sequence, _blockSize)};
}

void LinearComplexityTest::load(const std::string &config)
{
    XMLParse parser;
    parser.load(config, TestName);
    auto attr = parser.attribute("BlockSize");
    auto tmp = std::stoul(attr);
    if(tmp == 0)
    {
        throw std::logic_error("Block size must be positive");
    }
    _blockSize = tmp;
}

LinearComplexityTest::LinearComplexityTest(const std::string &config)
{
    load(config);
}
