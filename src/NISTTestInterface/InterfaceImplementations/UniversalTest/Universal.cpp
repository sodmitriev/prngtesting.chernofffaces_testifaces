//
// Created by svuatoslav on 12/15/18.
//

#include "Universal.h"
#include "stat_fncs.h"

std::vector<double> UniversalTest::runTest(const std::vector<bool> &sequence) const
{
    return {Universal(sequence)};
}