//
// Created by svuatoslav on 12/15/18.
//

#include "LongestRun.h"
#include "stat_fncs.h"

std::vector<double> LongestRunTest::runTest(const std::vector<bool> &sequence) const
{
    return {LongestRunOfOnes(sequence)};
}