cmake_minimum_required(VERSION 3.7)
project(niststiface)

set(CMAKE_CXX_STANDARD 17)

add_library(LongestRun ../include/LongestRun.h LongestRun.cpp)
target_link_libraries(LongestRun TestInterface niststpp)