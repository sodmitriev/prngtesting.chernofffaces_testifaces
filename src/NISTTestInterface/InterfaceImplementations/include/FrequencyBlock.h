//
// Created by svuatoslav on 12/15/18.
//

#ifndef NISTSTIFACE_FREQUENCYBLOCK_H
#define NISTSTIFACE_FREQUENCYBLOCK_H

#include "TestInterface.h"
#include <string>

class FrequencyBlockTest : public TestInterface
{
    int _blockSize = 0;
    static const char* TestName;
public:
    FrequencyBlockTest() = default;
    explicit FrequencyBlockTest(const std::string & config);
    ~FrequencyBlockTest() override = default;
    void load(const std::string & config);
    std::vector<double> runTest(const std::vector<bool> &sequence) const override;
};

#endif //NISTSTIFACE_FREQUENCYBLOCK_H
