//
// Created by svuatoslav on 12/15/18.
//

#ifndef NISTSTIFACE_RANK_H
#define NISTSTIFACE_RANK_H

#include "TestInterface.h"

class RankTest : public TestInterface
{
public:
    RankTest() = default;
    ~RankTest() override = default;
    std::vector<double> runTest(const std::vector<bool> &sequence) const override;
};

#endif //NISTSTIFACE_RANK_H
