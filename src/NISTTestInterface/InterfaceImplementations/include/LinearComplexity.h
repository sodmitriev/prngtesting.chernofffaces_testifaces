//
// Created by svuatoslav on 12/15/18.
//

#ifndef NISTSTIFACE_LINEARCOMPLEXITY_H
#define NISTSTIFACE_LINEARCOMPLEXITY_H

#include "TestInterface.h"
#include <string>

class LinearComplexityTest : public TestInterface
{
    unsigned long _blockSize = 0;
    static const char* TestName;
public:
    LinearComplexityTest() = default;
    explicit LinearComplexityTest(const std::string & config);
    ~LinearComplexityTest() override = default;
    void load(const std::string & config);
    std::vector<double> runTest(const std::vector<bool> &sequence) const override;
};

#endif //NISTSTIFACE_LINEARCOMPLEXITY_H
