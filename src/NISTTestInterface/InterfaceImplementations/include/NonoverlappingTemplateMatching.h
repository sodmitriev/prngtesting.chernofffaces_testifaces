//
// Created by svuatoslav on 12/15/18.
//

#ifndef NISTSTIFACE_NONOVERLAPPINGTEMPLATEMATCHING_H
#define NISTSTIFACE_NONOVERLAPPINGTEMPLATEMATCHING_H

#include "TestInterface.h"
#include <string>

class NonoverlappingTemplateMatchingTest : public TestInterface
{
    std::vector<std::pair<std::string, size_t>> _templates;
    size_t _blockSize = 0;
    static const char* TestName;
public:
    explicit NonoverlappingTemplateMatchingTest(const std::string & config);
    NonoverlappingTemplateMatchingTest() = default;
    ~NonoverlappingTemplateMatchingTest() override = default;
    void load(const std::string & config);
    std::vector<double> runTest(const std::vector<bool> &sequence) const override;
};

#endif //NISTSTIFACE_NONOVERLAPPINGTEMPLATEMATCHING_H
