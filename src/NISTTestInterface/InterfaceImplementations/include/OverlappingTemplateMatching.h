//
// Created by svuatoslav on 12/15/18.
//

#ifndef NISTSTIFACE_OVERLAPPINGTEMPLATEMATCHING_H
#define NISTSTIFACE_OVERLAPPINGTEMPLATEMATCHING_H

#include "TestInterface.h"
#include <string>

class OverlappingTemplateMatchingTest : public TestInterface
{
    std::vector<std::pair<std::string, size_t>> _templates;
    size_t _blockSize = 0;
    static const char* TestName;
public:
    explicit OverlappingTemplateMatchingTest(const std::string & config);
    OverlappingTemplateMatchingTest() = default;
    ~OverlappingTemplateMatchingTest() override = default;
    void load(const std::string & config);
    std::vector<double> runTest(const std::vector<bool> &sequence) const override;
};

#endif //NISTSTIFACE_OverlappingTemplateMatching_H
