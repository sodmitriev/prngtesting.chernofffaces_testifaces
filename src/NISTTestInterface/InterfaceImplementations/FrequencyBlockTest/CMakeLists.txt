cmake_minimum_required(VERSION 3.7)
project(niststiface)


set(CMAKE_CXX_STANDARD 17)

add_library(FrequencyBlock ../include/FrequencyBlock.h FrequencyBlock.cpp)
target_link_libraries(FrequencyBlock TestInterface niststpp XMLParse)