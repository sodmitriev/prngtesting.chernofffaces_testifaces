//
// Created by svuatoslav on 12/15/18.
//

#include <InterfaceImplementations/include/FrequencyBlock.h>

#include "stat_fncs.h"
#include "XMLParse.h"
#include "FrequencyBlock.h"

const char* FrequencyBlockTest::TestName = "FrequencyBlock";

std::vector<double> FrequencyBlockTest::runTest(const std::vector<bool> &sequence) const
{
    if(_blockSize <= 0)
    {
        throw std::logic_error( std::string(TestName) + " test is not loaded");
    }
    return {BlockFrequency(_blockSize, sequence)};
}

void FrequencyBlockTest::load(const std::string &config)
{
    XMLParse parser;
    parser.load(config, TestName);
    auto attr = parser.attribute("BlockSize");
    auto tmp = std::stoi(attr);
    if(tmp <= 0)
    {
        throw std::logic_error("Block size must be positive");
    }
    _blockSize = tmp;
}

FrequencyBlockTest::FrequencyBlockTest(const std::string &config)
{
    load(config);
}
