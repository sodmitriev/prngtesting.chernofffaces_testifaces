#pragma once
class BO_ResultsTest
{
	int TestNumber;
	double ResultTest;
public:
	BO_ResultsTest();
	BO_ResultsTest(int, double);
	~BO_ResultsTest();
	void SetResultTest(double);
	double GetResultTest();
	void SetTestNumber(int);
	int GetTestNumber();
};

