#pragma once
class BO_SequenceParameters
{
	char * FileName;
	unsigned long long int LengthSequence; // 2^64
	int NumberOfIterationsVer1;
	int NumberOfIterationsVer2;
	int Step;
public:
	BO_SequenceParameters();
	BO_SequenceParameters(char *, int, int, int, int);
	~BO_SequenceParameters();
	void SetFileName(char *);
	char * GetFileName();
	void SetLengthSequence(int);
	int GetLengthSequence();
	void SetNumberOfIterationsVer1(int);
	int GetNumberOfIterationsVer1();
	void SetNumberOfIterationsVer2(int);
	int GetNumberOfIterationsVer2();
	void SetStep(int);
	int GetStep();
};

