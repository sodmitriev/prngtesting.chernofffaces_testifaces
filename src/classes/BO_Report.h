#pragma once
#include "BO_SequenceParameters.h"
#include "BO_Face.h"
#include "BO_ResultsExperiment.h"

class BO_Report
{
	char * ReportName;
	double HushSum;
	BO_SequenceParameters Parameters;
	BO_ResultsExperiment * ResultSequenceVer1;
	BO_ResultsExperiment * ResultSequenceVer2;
	BO_Face Faces;
public:
	BO_Report();
	BO_Report(char *, double, BO_SequenceParameters, BO_ResultsExperiment *, BO_ResultsExperiment *, BO_Face);
	void SetReportName(char *);
	char * GetReportName();
	void SetHushSum(int);
	int GetHushSum();
	void SetParameters(BO_SequenceParameters);
	BO_SequenceParameters GetParameters();
	void SetResultSequenceVer1(BO_ResultsExperiment *);
	BO_ResultsExperiment * GetResultSequenceVer1();
	void SetResultSequenceVer2(BO_ResultsExperiment *);
	BO_ResultsExperiment * GetResultSequenceVer2();
	void SetFaces(BO_Face);
	BO_Face GetFaces();
	~BO_Report();
};

