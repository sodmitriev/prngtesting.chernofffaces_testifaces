// chernofffaces.cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.
//

#include "BO_Face.h"
#include "BO_FaceParametr.h"
#include "BO_Report.h"
#include "BO_ResultsExperiment.h"
#include "BO_ResultsIteration.h"
#include "BO_ResultsTest.h"
#include "BO_SequenceParameters.h"
#include <iostream>
#include <string>

int main()
{	
	int sizeVer1 = 4, sizeVer2 = 3;// количество итераций и размер массивов
	float resultVer1 = 0.0, resultVer2 = 0.0;
	int testnum = 0;
	int i = 0;

	BO_ResultsTest RT(1, 5.55);//Инициализация результата теста

	std::cout << "Res:" << RT.GetResultTest() << "\n";
	std::cout << "TestNumber:" << RT.GetTestNumber() << "\n";

	BO_ResultsTest RTforRIVer1[15], RTforRIVer2[15];// инициализируем результат теста для результатов итерации
	for (i = 0; i < 15; i++)
	{
		std::cout << "Testnum[" << i << "]:";
		std::cout << "Result[" << i << "]:";
		std::cin >> resultVer1;
		std::cout << "\n";
		resultVer2 = i+1;

		RTforRIVer1[i].SetResultTest(resultVer1);
		RTforRIVer1[i].SetTestNumber(i);

		RTforRIVer2[i].SetResultTest(resultVer2);
		RTforRIVer2[i].SetTestNumber(i);
	}
	// на выходе массив результатов итерации

	//BO_ResultsIteration RI(RTforRI);//Инициализация результатов итерации (передаем массив результатов всех тестов в результаты итерации)
	BO_ResultsIteration *resultsiterationVer1 = new BO_ResultsIteration[sizeVer1];
	BO_ResultsIteration *resultsiterarionVer2 = new BO_ResultsIteration[sizeVer2];
	BO_ResultsTest *RI1; //указатель типа ResultTest для получения массива

	resultsiterationVer1[0].SetResultIteration(RTforRIVer1);//Заполняемя массив результатов итерации для 1 варианта
	resultsiterarionVer2[0].SetResultIteration(RTforRIVer2);//Заполняемя массив результатов итерации для 2 варианта

	//RI1=resultsiterationVer1[0].GetResultIteration();// Получаем указатель на массив ResultIteration
	
	/*for (i = 0; i < 15; i++) //Вывод массива результатов
	{
		std::cout << "ResultVer1[" << i << "]:" << RI1[i].GetResultTest() << "\n";
		std::cout << "TestNumber[" << i << "]:" << RI1[i].GetTestNumber() << "\n";
	}

	RI1 = resultsiterarionVer2[0].GetResultIteration();// Получаем указатель на массив ResultIteration

	for (i = 0; i < 15; i++) //Вывод массива результатов
	{
		std::cout << "ResultVer2[" << i << "]:" << RI1[i].GetResultTest() << "\n";
		std::cout << "TestNumber[" << i << "]:" << RI1[i].GetTestNumber() << "\n";
	}*/

	BO_ResultsExperiment RE(resultsiterationVer1, sizeVer1); //инициализация
	BO_ResultsIteration  * RIforRE; // указатель типа ResultsIteration для получения массива
	RIforRE = RE.GetResultsExperiment();

	RI1 = RIforRE[0].GetResultIteration();
	for (i = 0; i < 15; i++) //Вывод массива результатов
	{
		std::cout << "TestNumber[" << i << "]:" << RI1[i].GetTestNumber() << "\n";
		std::cout << "ResultVer1[" << i << "]:" << RI1[i].GetResultTest() << "\n";
	}

	//SequenceParameters
	int lengthfile = 40;
	char * FName = new char[lengthfile];
	memset(FName, 0, sizeof(FName));
	strcpy_s(FName, 6, "File1");

	BO_SequenceParameters SP(FName, 100, 10, 8, 3);
	std::cout << "FileName" << SP.GetFileName() <<"\n";
	SP.SetFileName(FName);
	std::cout << "FileName2:" << SP.GetFileName() << "\n";

	int b = 0;
	std::cout << "GetResultTest:" << b << "\n";
	//report

	BO_ResultsTest RTforFaceParametr(2, 2.55); // ResultTest for Faceparametr
	BO_FaceParametr Faceparametr[15];// массив FaceParametr for Face
	BO_FaceParametr * GetFP;

	// заполняем массив  объектов Faceparametr 
	for (int i = 0; i < 15; i++) {
		Faceparametr[i].SetParametr(RTforFaceParametr);
		Faceparametr[i].SetNameParametr(FName);
	}
	BO_Face face(FName, Faceparametr);

	BO_Report report;
	report.SetParameters(SP);

}

// Запуск программы: CTRL+F5 или меню "Отладка" > "Запуск без отладки"
// Отладка программы: F5 или меню "Отладка" > "Запустить отладку"

// Советы по началу работы 
//   1. В окне обозревателя решений можно добавлять файлы и управлять ими.
//   2. В окне Team Explorer можно подключиться к системе управления версиями.
//   3. В окне "Выходные данные" можно просматривать выходные данные сборки и другие сообщения.
//   4. В окне "Список ошибок" можно просматривать ошибки.
//   5. Последовательно выберите пункты меню "Проект" > "Добавить новый элемент", чтобы создать файлы кода, или "Проект" > "Добавить существующий элемент", чтобы добавить в проект существующие файлы кода.
//   6. Чтобы снова открыть этот проект позже, выберите пункты меню "Файл" > "Открыть" > "Проект" и выберите SLN-файл.
