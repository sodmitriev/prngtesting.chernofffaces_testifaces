#pragma once
#include "BO_SequenceParameters.h"
#include "BO_ResultsExperiment.h"
#include "BO_Face.h"
#include "BO_Report.h"
#include <string>
#include <string.h>
#include <cstdlib>
#include <iostream>
#include <fstream>
#include <iostream>
#include <stdlib.h>
#include <math.h>

class BC_ControlComponent
{
public:
	BC_ControlComponent();
	~BC_ControlComponent();

	int ValidationOfParameters(BO_SequenceParameters *); //�������� ���������� 
	int OpeningFileAndParsing(BO_SequenceParameters * ); //�������� ����� � ������� ������������������
	int ExecutionStatisticalTests(BO_SequenceParameters *, BO_ResultsExperiment *, BO_ResultsExperiment *); // ���������� ������ ��� �������������� ������
	double * StatisticalTests(char *); //���������� �������������� ������
	int ReportGeneration(); //��������� ������
	int FindReport(); // ������ ������
	int SaveToDB(BO_ResultsExperiment *, BO_ResultsExperiment *); // �������� ��� ��
	int SaveToDB(BO_Face *, BO_Face *); // �������� ��� ��
	int GenerationFace(BO_Face *, BO_Face *); // ��������� ���
	int LoadingFromDB();
};

