#include "BC_ValidationOfParameters.h"
#include <string>
#include <iostream>

BC_ValidationOfParameters::BC_ValidationOfParameters(BO_SequenceParameters * SP)
{
	SeqParameters = SP;
}

BC_ValidationOfParameters::~BC_ValidationOfParameters()
{
}

int BC_ValidationOfParameters::GetValidationOfFileName()
{
	if (strlen(SeqParameters->GetFileName()) != 0)
	{
		return 1;
	}
	else
	{
		return 0;
	}

}

int BC_ValidationOfParameters::GetValidationOfStep()
{
	if (SeqParameters->GetStep() > 0)
	{
		return 1;
	}
	else
	{
		return 0;
	}
}

int BC_ValidationOfParameters::GetValidationOfLengthSequence()
{
	if (SeqParameters->GetLengthSequence() > 0)
	{
		return 1;
	}
	else
	{
		return 0;
	}
}
