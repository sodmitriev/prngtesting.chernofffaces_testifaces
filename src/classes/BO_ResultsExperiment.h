#pragma once
#include "BO_ResultsIteration.h"
#include "BO_SequenceParameters.h"

class BO_ResultsExperiment
{
	BO_ResultsIteration * ResultsExperiment;
public:
	BO_ResultsExperiment(BO_ResultsIteration *, int);
	~BO_ResultsExperiment();
	void SetResultsExperiment(BO_ResultsIteration *, int);
	BO_ResultsIteration * GetResultsExperiment();
	//void CallResultsIteration(BO_ResultsTest);
};

